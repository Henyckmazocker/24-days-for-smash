package smash.sash;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Comanda {
	protected int idComanda;
	protected Client client;
	protected Date dataComanda;
	protected Date dataLliurament;   
	protected ComandaEstat estat;	//PENDENT - PREPARAT - TRANSPORT - LLIURAT
	protected Double portes;		//preu de transport
	protected List <ComandaLinia> linies;
	
	Comanda() {
		idComanda = Generador.getNextComanda();
		dataComanda = new Date();
		dataLliurament = Tools.sumarDies(new Date(), 1);
		estat = ComandaEstat.PENDENT;
		portes = 0.0;
		linies = new ArrayList<ComandaLinia>();
	}

	Comanda(Client client) {
		this();
		this.client = client;
	};	
	

	List <ComandaLinia> getLinies (){
		return linies;
	}
}